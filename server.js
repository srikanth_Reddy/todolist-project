const fs = require('fs')
var express = require('express');
var app = express();
const path = require('path');
var cors = require('cors');
app.use(express.json());
app.use(express.urlencoded({ extended: false }))
app.use(cors());

// upload data when restart
app.get('/load', (req, res) => {
    fs.promises.readFile('./tasks.json', ('utf-8'))
        .then((result) => {
            let data = JSON.parse(result)
            res.json(data);
        })
        .catch((err) => {
            res.status(400);
            res.json({ msg: "upload failed" })
        })
})


// add & remove tasks from database
app.post('/update', (req, res) => {
    fs.promises.readFile('./tasks.json', ('utf-8'))
        .then((result) => {
            let dataaa = JSON.parse(result);
            if (req.body.operation === "add") {
                dataaa.push(req.body.newTask)
            }
            else if (req.body.operation === "remove") {
                let ind = dataaa.indexOf(req.body.newTask);
                dataaa = ((dataaa.slice(0, ind)).concat(dataaa.slice(ind + 1, dataaa.length)));
            }
            fs.promises.writeFile("./tasks.json", JSON.stringify(dataaa), ('utf-8'))
                .then(() => {
                    res.json({ "msg": "success" });
                })
                .catch((err) => {
                    res.status(400);
                    res.json({ "msg": "adding tasks to databack" })
                })

        })
        .catch((err) => {
            res.status(400);
            res.json({ "msg": "file reading failed" })
        })
})


app.listen(5000, () => {
    console.log("server started at 5000");
})