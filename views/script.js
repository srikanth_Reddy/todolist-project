//Load the database content
const options = {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    },

}
fetch('http://localhost:5000/load', options)
    .then((data) => data.json())
    .then((data) => {
        data.forEach((task) => {
            addTask(task)
        })
    })


let form = document.querySelector("form");
let checkbox = document.querySelectorAll('input[type="checkbox"]')
let taskErr = document.getElementById("taskErr");
let tasks = document.getElementById("tasks");
let lstOfTask = document.querySelectorAll(".task");
let msg = document.getElementById('msg');
form.addEventListener("submit", () => {
    event.preventDefault();
    let newTask = (event.target.newTask.value).toUpperCase();
    if (newTask.length > 0) {
        addTask(newTask);
        updateTaskInDatabase(newTask, "add");
    }
});


tasks.addEventListener("click", operation);

let taskNumber = document.getElementById("num")

taskNumber.textContent = (document.querySelectorAll(".task")).length;


function operation(event) {
    // remove operation
    if (event.target.type === 'button') {
        tasks.removeChild(event.target.parentElement);
        console.log(event.target.previousSibling.textContent)
        // Updating count of tasks
        taskNumber.textContent = (document.querySelectorAll(".task")).length;

        //update tasks in database
        updateTaskInDatabase(event.target.previousSibling.textContent, "remove");
    }
    //check box operation
    if ((event.target).checked) {
        event.target.parentElement.parentElement.style.backgroundColor = "green";
    }
    else if ((event.target).checked == false) {
        event.target.parentElement.parentElement.style.backgroundColor = "";
    }
}

// adding tasks to frontEnd
function addTask(newTask) {
    let button = document.createElement("button");
    button.setAttribute("type", "button");
    let x = document.createTextNode("X");
    button.appendChild(x);

    input = document.createElement("input");
    input.setAttribute("type", "checkbox");

    div2 = document.createElement("div");
    div2.appendChild(input);
    let a = document.createTextNode(newTask);
    div2.appendChild(a);

    let div1 = document.createElement("div");
    div1.className = "task";
    div1.appendChild(div2)
    div1.appendChild(button)

    tasks.appendChild(div1)

    //UpDating The Count Of Tasks
    taskNumber.textContent = (document.querySelectorAll(".task")).length;

}

//Update database
function updateTaskInDatabase(newTask, operation) {
    let data = { newTask, operation };
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }
    fetch('http://localhost:5000/update', options)
        .then((data) => data.json())
        .then((data) => {
            msg.textContent = data.msg;
        })
}
